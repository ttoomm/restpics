require(["dijit/layout/AccordionContainer", "dijit/layout/AccordionPane", "dojo/dom", 
	 "dojo/dom-construct", "app/Dish",
	 "dojo/store/JsonRest", "dojo/_base/lang", "dojox/data/CouchDBRestStore",
	 "dojo/domReady!"],
        function(AccordionContainer, ContentPane, dom,  domConst, Dish, Rest, lang, Couchdb){
	

		
		var myStore = new Couchdb({target:"/"});
		
	    var store=new Rest({
		target: "./rest/menu/catagories/"
	    });

	    var dishStore=new Rest({
		target: "./rest/menu/dishes/"
	    });

    	    var div=dom.byId("div");
   	    var aContainer = new AccordionContainer({style:"height: 300px"}, div);


	    var showDishes=function(dishes, cp){
		console.debug(dishes.length);
		console.debug(dishes.join(","));
		var domDishes=domConst.create("div", {style: "display: inline;"});
		
		for(var i=0; i<dishes.length; i++){
		    console.debug("start query: "+dishes[i]);
		    dishStore.query(""+dishes[i]).then(lang.partial(function(domNode, dish){
			var wdish=new Dish({dish:dish});
			wdish.placeAt(domNode);
		    }, domDishes));
		}
		cp.set("content", domDishes);
		
	    }

	    var showCats=function(total){
		
		    for(var i=0; i<total; i++){
			store.query(""+i).then(function(cat){
			console.debug(cat);
			var cp=new ContentPane({
			    title: "["+cat.name+"] "+cat.desc,
			    content: "dishes"
			});
			cp.set("onShow", lang.partial(showDishes, cat.dishes, cp));
			aContainer.addChild(cp);
		    });

		}
	    }

	    
	    

	    store.query("total").then(
		function(total){
		    console.debug(total);
		    showCats(total);

		}).then(function(){
    		    aContainer.startup();
		});
	    
})
