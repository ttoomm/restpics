package dataPopulator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;

import tom.h.model.DishModel;

import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Document;
import com.fourspaces.couchdb.Session;

public class DataPopulator {
	
	
	

	String[][] getChickenDishes() {
		return new String[][] {
				// -------name
				new String[] { "Honey Sesame Chicken", "Orange Chicken",
						"SweetFire Chicken Breast",
						"String Bean Chicken Breast", "Kung Pao Chicken",
						"Mushroom Chicken", "Black Pepper Chicken",
						"Grilled Teriyaki Chicken", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-honeysesame.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-orange.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-sweetfire.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-stringbean.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-kungpao.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-mushroom.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-blackpepper.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-grilledteriyaki.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/wok-chicken-honeysesame.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-orange.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-sweetfire.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-stringbean.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-kungpao.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-mushroom.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-blackpepper.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-chicken-grilledteriyaki.jpg", },
				new String[] {
						"Honey Sesame Chicken Breast is made with thin, crispy strips of all-white meat chicken tossed with fresh-cut string beans, crisp yellow bell peppers in a sizzling hot wok with our new delicious honey sauce and topped off with sesame seeds.",
						"Orange Chicken is a dish inspired by the Hunan Province in South Central China. It is prepared with crispy boneless chicken bites, tossed in the wok with our secret sweet and spicy orange sauce. Panda's very own Executive Chef Andy brought this entree to life and it quickly became Panda's most beloved dish.",
						"SweetFire Chicken Breast features crispy, white-meat chicken bites tossed in the wok with red bell peppers, diced onions and juicy pineapple with a zesty, sweet chili sauce inspired by the flavors of Thailand.",
						"String Bean Chicken Breast is a Cantonese dish inspired by the Guangdong Province on the Southern Coast of China. It is prepared with marinated chicken breast, fresh-cut string beans and sliced onions, tossed in the wok with a mild ginger soy sauce.",
						"Kung Pao Chicken is a spicy stir-fry dish inspired by the Sichuan Province in Central China. It is prepared with marinated diced chicken, crunchy peanuts, diced red bell peppers and sliced zucchini, all tossed in the wok with fresh green onions.",
						"Mushroom Chicken is a Cantonese dish inspired by the Guangdong Province on the Southern Coast of China. It is prepared with marinated diced chicken, sliced zucchini and button mushrooms, all tossed in the wok with a mild ginger soy sauce.",
						"Black Pepper Chicken is a traditional dish inspired by the Hunan Province in South Central China. It is prepared with marinated diced chicken, chopped celery, sliced onions and fresh ground black pepper, tossed in the wok with a mild ginger soy sauce.",
						"Grilled Teriyaki Chicken features marinated chicken filets that are grilled to perfection then sliced to order and served with our sweet and savory sauce. Availability of Grilled Teriyaki Chicken may vary by location.  View our Nutritional & Allergen PDF for details."

				} };
	}
	
	float[][] getChickenNutritions(){
		return new float[][]{
				new float[]{4,320,150,17,3,0,30,360,30,2,14,12},
				new float[]{4,1,290,120,13,2,5,0,60,450,32,0,14,10},
				new float[]{5,8,440,140,15,3,0,35,320,47,1,27,13},
				new float[]{5,6,160,120,13,2,5,0,40,730,13,2,6,15},
				new float[]{4,4,190,120,14,2,5,0,40,690,10,2,4,11},
				new float[]{4,4,130,100,11,2,0,40,650,8,0,4,10},
				new float[]{4,6,140,120,14,2,5,0,40,830,11,1,5,10},
				new float[]{3,6,180,70,8,2,5,0,110,320,5,0,5,22},
		};
	}
	
	boolean[][] getChickenAllergens(){ return new boolean[][]{
			new boolean[]{true,true,false,false,false,false,false,false},
			new boolean[]{true,true,false,false,false,false,true,true},
			new boolean[]{true,true,false,false,false,false,false,false},
			new boolean[]{true,true,false,false,false,false,false,false},
			new boolean[]{true,true,true,false,false,false,false,false},
			new boolean[]{true,true,false,false,false,false,false,false},
			new boolean[]{true,true,false,false,false,false,false,false},
			new boolean[]{true,true,false,false,false,false,false,false},
			};}
	
	

	String[][] getBeefDishes() {
		return new String[][] {
				// -------name
				new String[] { "Broccoli Beef", "Shanghai Angus Steak",
						"Beijing Beef", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-beef-broccoli.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-beef-shanghaiangussteak.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-beef-beijing.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-beef-broccoli.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-steak-shanghaiasian.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-beef-beijing.jpg", },
				new String[] {
						"Broccoli Beef is a Cantonese dish inspired by the Guangdong Province on the Southern Coast of China. It is prepared with marinated sliced beef and fresh broccoli florets, tossed in the wok with a mild ginger soy sauce.",
						"Shanghai Angus Steak features thick-cut slices of marinated Angus Top Sirloin with crisp asparagus, freshly sliced mushrooms, all wok-tossed in our new zesty Asian steak sauce.",
						"Beijing Beef is a Sichuan-style dish inspired by Central China. It is prepared with crispy strips of marinated beef, bell peppers and sliced onions, tossed in the wok with a tangy sweet and spicy sauce."

				}

		};
	}
	
	float[][] getBeefDishesNutritions(){
		return new float[][]{
				new float[]{4,1,100,60,7,1,0,10,500,9,2,3,7},
				new float[]{4,1,170,130,14,3,0,40,630,12,1,8,17},
				new float[]{4,2,520,240,26,5,0,40,580,44,3,24,16},

				
		};
	}
	
	boolean[][] getBeefDishesAllergens(){	return new boolean[][]{
			new boolean[]{true,true,false,false,false,false,false,false},
			new boolean[]{true,true,false,false,false,false,false,false},
			new boolean[]{true,true,false,false,false,false,false,true},
			};}

	String[][] getSeafoodDishes() {
		return new String[][] {
				// -------name
				new String[] { "Honey Walnut Shrimp", },
				// -------icon
				new String[] { "https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-shrimp-honeywalnut.png", },
				// -------pic
				new String[] { "https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-shrimp-honeywalnut.jpg", },
				new String[] { "Honey Walnut Shrimp is a dish inspired by the shores of Shanghai. It features fresh tempura shrimp wok-tossed in a gourmet honey sauce and topped with glazed walnuts. It's a mouthwatering combination of sweet and crispy." }

		};
	}
	float[][] getSeafoodNutritions(){
		return new float[][]{
				new float[]{2,200,110,13,2,0,55,240,14,1,5,7},

			
		};
	}
	boolean[][] getSeafoodAllergens(){
		 return new boolean[][]{
				new boolean[]{true,true,false,true,false,true,true,true},
				
				};
	}

	String[][] getRegionEntires() {
		return new String[][] {
				// -------name
				new String[] { "Eggplant Tofu", },
				// -------icon
				new String[] { "https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-other-eggplanttofu.png", },
				// -------pic
				new String[] { "https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok_other_eggplanttofu.jpg", },
				new String[] { "Eggplant Tofu is a dish inspired by the Sichuan Province in Central China prepared with lightly browned tofu, fresh eggplant and diced red bell peppers, tossed in the wok with a sweet and spicy sauce." }

		};
	}
	float[][] getRegionEntiresNutritions(){
		return new float[][]{
				new float[]{6,1,310,210,24,3,5,0,0,520,23,3,17,7},

				
		};
	}
	
	boolean[][] getRegionEntiresAllergens(){return new boolean[][]{
			new boolean[]{true,true,false,false,false,false,false,false},
			};}

	String[][] getSidesDishes() {
		return new String[][] {
				// -------name
				new String[] { "Chow Mein", "Fried Rice", "Mixed Veggies",
						"White Steamed Rice", "Brown Steamed Rice", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-chowmein.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-friedrice.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-mixedveggies.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-steamedrice.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-steamedbrownrice.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-side-chowmein.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-sides-friedrice.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-sides-mixedveggies.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-sides-steamedrice.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-sides-steamedbrownrice.jpg", },
				new String[] {
						"Chow Mein is prepared with our noodles, tossed in the wok with shredded onions, crisp celery and fresh cabbage.",
						"Fried Rice is prepared with steamed white rice that is tossed in the wok with soy sauce, scrambled eggs, green peas, carrots and chopped green onions.",
						"Mixed Veggies is a stir-fry combination of fresh broccoli, zucchini, carrots, string beans and cabbage.",
						"White Steamed Rice is prepared by steaming white rice to perfection.",
						"Brown Steamed Rice is prepared by steaming brown rice to perfection." }

		};
	}
	
	float[][] getSidesDishesNutritions(){
		return new float[][]{
				

				new float[]{9,4,490,200,22,4,0,0,980,65,4,9,13},
				new float[]{9,3,530,140,16,3,0,150,790,82,1,3,12},
				new float[]{8,6,70,5,0,5,0,0,0,540,16,5,5,4},
				new float[]{8,1,380,0,0,0,0,0,0,87,0,0,7},
				new float[]{10,4,420,35,4,1,0,0,15,86,4,1,9},

				
		};
	}
	
	boolean[][] getSidesDishesAllergens(){
		return new boolean[][]{
				new boolean[]{true,true,false,false,false,false,false,false},
				new boolean[]{true,true,false,false,false,false,true,false},
				new boolean[]{true,true,false,false,false,false,false,true},
				new boolean[]{false,false,false,false,false,false,false,false},
				new boolean[]{false,false,false,false,false,false,false,false},
				};
	}

	String[][] getAppettizers() {
		return new String[][] {
				// -------name
				new String[] { "Chicken Egg Roll", "Veggie Spring Roll",
						"Hot and Sour Soup", "Chicken Potsticker",
						"Crispy Shrimp", "Cream Cheese Rangoon", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-ckeggrolls.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-vgspringrolls.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-hotsoursoup.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-ckpotstickers.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizer-crispyshrimp.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-ccrangoons.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-appetizers-ckeggrolls.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-appetizers-vgspringrolls.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bowl-appetizers-hotsoursoup.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-appetizers-ckpotstickers.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-shrimp-crispy.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-appetizers-ccrangoons.jpg", },
				new String[] {
						"Chicken Egg Rolls are prepared with a mixture of cabbage, carrots, mushrooms, green onions and marinated chicken wrapped in a thin wonton wrapper and cooked to a golden brown.",
						"Veggie Spring Rolls are prepared with a mixture of cabbage, celery, carrots, green onions and Chinese noodles wrapped in a thin wonton wrapper and cooked to a golden brown.",
						"Hot & Sour Soup is a traditional Chinese soup prepared with vegetable stock, eggs, tofu and button mushrooms.",
						"Chicken Potstickers are prepared with a soft white dumpling filled with a combination of chicken, cabbage and onions that is pan seared on one side to a golden brown.",
						"Crispy Shrimp is prepared with marinated, butterflied shrimp that are cooked to a crispy golden brown.",
						"Cream Cheese Rangoons are prepared with a crisp wonton wrapper filled with a mixture of soft cream cheese and green onions, served with a side of sweet and sour sauce for dipping." }

		};
	}
	
	float[][] getAppettizersNutritions(){
		return new float[][]{
				

				

				new float[]{3,200,90,10,2,0,20,340,20,2,2,6},
				new float[]{3,4,160,80,8,1,5,0,0,520,27,2,3,3},
				new float[]{12,15,100,40,4,5,0,5,0,65,880,14,1,4,7},
				new float[]{3,3,220,60,6,1,5,0,20,250,20,1,2,6},
				new float[]{3,5,260,120,13,2,0,60,800,26,1,2,9},
				new float[]{2,4,190,70,8,5,0,35,180,24,2,1,5},

				
		};
	}
	
	boolean[][] getAppettizersAllergens(){ return new boolean[][]{
			new boolean[]{true,true,false,false,false,false,true,true},
			new boolean[]{true,true,false,false,false,false,false,true},
			new boolean[]{true,true,false,false,false,false,true,true},
			new boolean[]{true,true,false,false,false,false,false,false},
			new boolean[]{true,true,false,false,false,true,false,true},
			new boolean[]{true,false,false,false,false,false,true,true},
			};}

	String[][] getDesserts() {
		return new String[][] {
				// -------name
				new String[] { "Fortune Cookies", "Chocolate Chunk Cookie", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-desserts-fortunecookies.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-desserts-chocolatechunkcookies.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-desserts-fortunecookies.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-desserts-chocolatechunkcookies.jpg", },

		};
	}
	float[][] getDessertsNutritions(){
		return new float[][]{
				
	new float[]{0,3,32,2,0,0,0,0,8,7,0,3,1},
	new float[]{0,3,32,2,0,0,0,0,8,7,0,3,1},

	};
	}
	
	boolean[][] getDessertsAllergens(){
		return new boolean[][]{
				new boolean[]{true,true,false,false,false,false,false,true},
				new boolean[]{true,true,false,false,false,false,false,true},
				};
	}
	

	String[][] getDrink() {
		return new String[][] {
				// -------name
				new String[] { "Pepsi", "Diet Pepsi", "Dr Pepper",
						"Sierra Mist", "Mountain Dew", "Mug Root Beer",
						"Sobe Lean", "Lipton Brisk Raspberry",
						"Lipton No Calorie Brisk Peach", "Tropicana Lemonade",
						"Tropicana Pink Lemonade", "Tropicana Fruit Punch",
						"Aquafina", "Gatorade Lemon-Lime",
						"Izze Sparkling Blackberry", "Ocean Spray Apple Juice",
						"SoBe Green Tea", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-pepsi.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-dietpepsi.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-drpepper.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-sierramist.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-mountaindew.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-mugrootbeer.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-sobecranberry.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-briskraspberryicedtea.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-briskpeachgreen.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-tropicanalemonade.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-tropicanapinklemonade.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-tropicanafruitpunch.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-aquafina.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-gatorade.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-izzejuice.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-oceansprayapplejuice.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-sobegreentea.png", },
				// -------pic
				new String[]{
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-pepsi.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-dietpepsi.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-drpepper.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-sierramist.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-mountaindew.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-mugrootbeer.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-sobecranberry.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-briskraspberryicedtea.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-briskpeachgreen.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-tropicanalemonade.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-tropicanapinklemonade.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-tropicanafruitpunch.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-aquafina.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-gatorade.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-izzejuice.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-oceansprayapplejuice.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-sobegreentea.jpg",
						},
		};

	}
	
	float[][] getDrinkNutritions(){
		return new float[][]{
	
	new float[]{30,330,0,0,0,0,0,80,92,0,89,0},
	new float[]{30,0,0,0,0,0,0,80,0,0,0,0},
	new float[]{30,281,0,0,0,0,0,98,76,0,76,0},
	new float[]{30,330,0,0,0,0,0,65,89,0,89,0},
	new float[]{42,510,0,0,0,0,0,160,133,0,133,0},
	new float[]{30,330,0,0,0,0,0,50,85,0,85,0},
	new float[]{30,0,0,0,0,0,0,80,0,0,0,0},
	new float[]{30,260,0,0,0,0,0,80,69,0,69,0},
	new float[]{30,0,0,0,0,0,0,230,0,0,0,0},
	new float[]{30,330,0,0,0,0,0,345,89,0,89,0},
	new float[]{30,330,0,0,0,0,0,345,89,0,89,0},
	new float[]{30,360,0,0,0,0,0,80,98,0,98,0},
	new float[]{42,460,0,0,0,0,0,480,124,0,124,0},
	new float[]{30,330,0,0,0,0,0,345,89,0,89,0},
	new float[]{22,240,0,0,0,0,0,255,65,0,65,0},
	new float[]{22,240,0,0,0,0,0,255,65,0,65,0},
	new float[]{22,240,0,0,0,0,0,0,0,0,0,0}
		};
	}
	
	boolean[][] getDrinkAllergens(){	 return new boolean[][]{
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},
			new boolean[]{false,false,false,false,false,false,false,false},

	};}

	String[] getCatagories() {
		return new String[] { "Chicken Entrees", "Beef Entrees",
				"Seafood Entrees", "Regional Entrees", "Sides", "Appetizers",
				"Desserts", "Drinks" };

	}
	
	
	List<Dish> dishes=new ArrayList<Dish>();
	List<Catagory> cats=new ArrayList<Catagory>();
	
//	public void genNutritionAndAllergen(Dish dish){
//		for(DishModel.Allergen a: DishModel.Allergen.values()){
//			dish.allergens.put(a, Math.random()>0.5);
//		}
//		for(DishModel.Nutrition n: DishModel.Nutrition.values()){
//			dish.nutritions.put(n, (float)Math.random()*10);
//		}
//	}
	
	void initData(){
		String[] strCats = this.getCatagories();
		String[][][] dishesData = new String[][][] { this.getChickenDishes(),
				this.getBeefDishes(), this.getSeafoodDishes(), this.getRegionEntires(),
				this.getSidesDishes(), this.getAppettizers(), this.getDesserts(),
				this.getDrink() };
		boolean[][][] allergens=new boolean[][][]{
				this.getChickenAllergens(),
				this.getBeefDishesAllergens(), this.getSeafoodAllergens(), this.getRegionEntiresAllergens(),
				this.getSidesDishesAllergens(), this.getAppettizersAllergens(), this.getDessertsAllergens(),
				this.getDrinkAllergens()
		};
		float[][][] nutritions=new float[][][]{
				this.getChickenNutritions(),
				this.getBeefDishesNutritions(), this.getSeafoodNutritions(), this.getRegionEntiresNutritions(),
				this.getSidesDishesNutritions(), this.getAppettizersNutritions(), this.getDessertsNutritions(),
				this.getDrinkNutritions()
				
		};
		
		
		
		for (int i = 0; i < strCats.length; i++) {
			String nameCat = strCats[i];
			List<Integer> dishIds=new ArrayList<Integer>();
			

			for (int j = 0; j < dishesData[i][0].length; j++) {

				int idDish=i*10+j;
				String name = dishesData[i][0][j];
				String icon = dishesData[i][1][j];
				String pic = dishesData[i][2][j];
				String desc = dishesData[i].length == 3 ? ""
						: dishesData[i][3][j];

				// assume the price is between $1 to $10
				int price = (int) (Math.random() * 1000);
				Dish dish = new Dish(idDish, name, desc, icon, pic, price);
				System.out.println("create dish "+name);
				dish.nutritions=new HashMap<DishModel.Nutrition, Float>();
				
				dish.allergens=new HashMap<DishModel.Allergen, Boolean>();
				
				int ai=0;
				for(DishModel.Allergen a: DishModel.Allergen.values()){
					
					dish.allergens.put(a, allergens[i][j][ai++]);
				}
				ai=0;
				for(DishModel.Nutrition a: DishModel.Nutrition.values()){

					dish.nutritions.put(a, nutritions[i][j][ai++]);
				}
				
				
				dishIds.add(idDish);
				this.dishes.add(dish);
			}
			
			Catagory cat=new Catagory(i, nameCat, "", "", dishIds);
			System.out.println("create catagory "+nameCat);
			this.cats.add(cat);

		}

	}
	
	void populateDb(String dbname) throws IOException{
		
		
		Session se = new Session("localhost",5984);
		List<String> dbs=se.getDatabaseNames();
		if(dbs.contains(dbname)){
			System.out.println("database exist, drop the database? (y/n)");
			byte[] buf=new byte[1024];
			System.in.read(buf);
			String ans=new String(buf);
			System.out.println("answer: "+ans);
			if(ans.trim().equals("y")){
				System.out.println("deleting db");
				se.deleteDatabase(dbname);
			}else{
				System.out.println("keep db");
				return;
			}
		}
		
		Database db = se.createDatabase(dbname);
				
		Document[] docs=new Document[this.cats.size()+this.dishes.size()];
		int index=0;
		
		
		for(int i=0; i<this.cats.size(); i++){
			docs[index]=this.cats.get(i).getDocument();
			index++;
		}
		for(int i=0; i<this.dishes.size(); i++){
			docs[index]=this.dishes.get(i).getDocument();
		
			index++;
		}
		
		db.bulkSaveDocuments(docs);
	}
	
	
	
	
	
	
	

	public static void main(String[] args) throws IOException, MethodNotSupportedException {

		DataPopulator p = new DataPopulator();
		p.initData();
		//p.showDish();
		p.populateDb("zm");
		
		

		
	}

}
