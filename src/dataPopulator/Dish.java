package dataPopulator;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;

import org.apache.tomcat.util.codec.binary.Base64;


import java.util.Map;

import net.sf.json.JSONObject;
import tom.h.model.DishModel;

import com.fourspaces.couchdb.Document;



public class Dish {
	public int id=0;

	public String name="";
	public String desc="";
	public int price=0;
	public String icon="";
	public String pic="";
	//public NutritionFact nutritions;
	//public Allergen allergens;
	public Map<DishModel.Nutrition, Float> nutritions;
	public Map<DishModel.Allergen, Boolean> allergens;
	
	public String type;
	public int cata;
	

	
	private String saveFile(String url) throws IOException {
		
		InputStream is = new URL(url).openStream();

		int bufSize=1024*1024*5;
		byte[] _buf = new byte[bufSize];
		int total=0;
		for (int i = 0; i >= 0 && total<bufSize ; i = is.read(_buf, total, bufSize-total)) {
			total+=i;
		}
		byte[] buf=Arrays.copyOf(_buf, total);
		is.close();
		
		String s=Base64.encodeBase64String(buf);
		
		
		return s;
	}
	
	public Dish() {
		this.type=this.getClass().getCanonicalName();
		
	}
	
	public Dish(int id, String name, String desc,  String icon, String pic, int price){
		this();
		this.id=id;
		this.name=name;
		this.desc=desc;
		this.price=price;
		try {
			this.icon=this.saveFile(icon);
			//System.out.println("icon: "+this.icon);
			this.pic=this.saveFile(pic);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	private JSONObject allergensToJsonObject(Map<DishModel.Allergen, Boolean> m){
		System.out.format("allergens: %s\n", m.keySet());
		JSONObject jo=new JSONObject();
		for(DishModel.Allergen o: m.keySet()){
			
			jo.put(o, m.get(o));
		}
		return jo;
	}
	
	private JSONObject nutritionToJo(Map<DishModel.Nutrition, Float> m){
		JSONObject jo=new JSONObject();
		System.out.format("nutrition: %s\n", m.keySet());
		for(DishModel.Nutrition o: m.keySet()){
			jo.put(o.toString(), m.get(o));
		}
		return jo;
	}
	
	public Document getDocument(){
		Document doc=new Document();
		
		doc.put("id", this.id);
		doc.put("desc", this.desc);
		doc.put("name", this.name);
		doc.put("price", this.price);
		doc.put("type", this.type);
		doc.put("icon", this.icon);
		doc.put("pic", this.pic);
		
		System.out.format("dish: %s \n mapping: \n",  name);
		doc.put("nutritions", this.nutritionToJo(this.nutritions));
		doc.put("allergens", this.allergensToJsonObject(this.allergens));
		

		return doc;
	}
	
//	public String getNutritions() throws JsonProcessingException{
//		return new ObjectMapper().writeValueAsString(this.nutritions);
//	}
//	
//	public String getAllergens() throws JsonProcessingException{
//		return new ObjectMapper().writeValueAsString(this.allergens);
//	}
	
	
}
