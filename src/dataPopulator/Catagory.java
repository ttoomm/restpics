package dataPopulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.fourspaces.couchdb.Document;



public class Catagory {
	public int id=0;
	public String name="";
	public String desc="";
	public String pic="";
	public List<Integer> dishes;
	public String type;
	
	public Catagory(){
		this.dishes=new ArrayList<Integer>();
		this.type=this.getClass().getCanonicalName();
	}
	public Catagory(int id, String name, String desc, String pic, List<Integer> dishes){
		this();
		this.id=id;
		this.name=name;
		this.desc=desc;
		this.pic=pic;
		this.dishes=dishes;
	}
	
	public Document getDocument(){
		Document doc=new Document();
		doc.put("id", this.id);
		doc.put("name", this.name);
		doc.put("desc", this.desc);
		doc.put("type", this.type);
		doc.put("dishes", this.dishes.toString());
		return doc;
	}
	
	
}
