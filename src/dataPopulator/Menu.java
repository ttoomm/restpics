package dataPopulator;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path("/menu")
public class Menu {
	
	static List<Catagory> cats;
	static List<Dish> dishes;
	
	
	final static String pp="/home/hx/git/restPics/WebContent";
	final static String JSON_Catagory=pp+"/catagories.json"; 
	final static String JSON_Dishes=pp+"/dishes.json"; 
	
	
	//private UriInfo uri;
	@Context 
	ServletContext context;
	
	
	String value;

	
	public Menu() throws JsonParseException, JsonMappingException, IOException, NamingException{
		if(cats==null){
			
			System.out.println("init data");
			
			this.initiateData();
		}
			
	}
	
	
	public static class C{
		public C(){
			this.catagories=new ArrayList<Catagory>();
		}
		public List<Catagory> catagories;
	};
	public static class D{
		public D(){
			this.dishes=new ArrayList<Dish>();
		}
		public List<Dish> dishes;
	}

	void initiateData() throws JsonParseException, JsonMappingException, IOException, NamingException{
		
		

		InputStream catCfg = new FileInputStream(JSON_Catagory);
		InputStream dishCfg = new FileInputStream(JSON_Dishes);
		
		BufferedReader r=new BufferedReader(new FileReader(JSON_Dishes));
		
		for(String s=""; s!=null; s=r.readLine()){
			System.out.println(s);
		}
		
		
		
		D dd=new D();
		//dd.dishes.add(new Dish(2, "nn", "dd",  "uu", "uu", 22, 2));
		
		
		
		
		
		
		ObjectMapper mapper=new ObjectMapper();
		System.out.println(mapper.writeValueAsString(dd));
		
		
		C c=mapper.readValue(catCfg, C.class);
		D d=mapper.readValue(dishCfg, D.class);
		
		cats=c.catagories;
		dishes=d.dishes;
		
		
		
		/*

		Menu.cats=new ArrayList<Catagory>();
		Menu.dishes=new ArrayList<Dish>();
		for(int i=0; i<3; i++){
			List<Dish> _dishes=new ArrayList<Dish>();
			for(int j=0; j<4; j++){
				int id=i*10+j;
				Dish dish=new Dish(id, "dish"+id, "desc"+id, id*100, "pic"+id);
				_dishes.add(dish);
			}
			Menu.dishes.addAll(_dishes);
			Catagory cc=new Catagory(i, "cat"+i, "desc"+i, "pic"+i, _dishes);
			cats.add(cc);
		}
		*/
	
	}
	
	@GET
	@Path("/catagories")
	@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public String getCatagories() throws JsonProcessingException{

		ObjectMapper mapper=new ObjectMapper();
		String s=mapper.writeValueAsString(cats);
		return s;
	}
	
	@GET
	@Path("/catagories/total")
	@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public String getCatagoriesTotal() throws JsonProcessingException{
		return ""+Menu.cats.size();
	}

	
	@GET
	@Path("/catagories/{id}")
	@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public String getCatagories(@PathParam("id") String strId) throws JsonProcessingException{

		if(strId==null || !strId.matches("^\\d+$")){
			return "";
		}
		
		
		int id=Integer.parseInt(strId);
		
		for(Catagory c: cats){
			if(c.id==id){
				ObjectMapper mapper=new ObjectMapper();
				String s=mapper.writeValueAsString(c);
				System.out.println("return: "+s);
				return s;
			}
		}
		
		return "";
	}

	
	@GET
	@Path("/dishes")
	@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public String getDishes() throws JsonProcessingException{
		ObjectMapper mapper=new ObjectMapper();
		return mapper.writeValueAsString(Menu.dishes);

	}
	
	
	@GET
	@Path("/dishes/{id}")
	@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public String getDish(@PathParam("id") String strId) throws JsonProcessingException{
		if(null==strId || !strId.matches("^\\d+$")) return "";
		
		int id=Integer.parseInt(strId);
		
		for (Dish d : Menu.dishes) {
			if (d.id == id) {
				ObjectMapper mapper=new ObjectMapper();
				return mapper.writeValueAsString(d);
			}
		}

		return "";
		
	}

}
